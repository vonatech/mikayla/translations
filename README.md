Please, read the [CONTRIBUTING.md](https://gitlab.com/muonium/mikayla/translations/blob/master/CONTRIBUTING.md) file before doing anything

Thanks to all the [people](https://gitlab.com/muonium/mikayla/translations/graphs/contributors) who have contributed!

Don't hesitate to join our Translations Project group chat [here](https://t.me/mui_localizations).
